# Minesweeper

This game was created using Angular 5 and packed as desktop app for Linux, Windows & OS X using Electron.

Feel free to try it by running:
```
npm i
npm run electron-build
npm run pack-win    # For Windows
npm run pack-mac    # For OS X
npm run pack-lin    # For Linux
```